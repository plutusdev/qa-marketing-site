<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- made by www.metatags.org -->
    <meta name="description" content="Nedbank , Nedbank PocketPOS, nedbank pocketPOS, nedbank masterpass, nedbank, nedBank" />
    <meta name="keywords" content="Nedbank , nedbank, Nedbank PocketPOS, nedbank, See money Differently , Functional design, Apply now for Nedbank PocketPOS" />
    <meta name="author" content="metatags generator">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="3 month">
    <title>NedBank</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
    <link rel="manifest" href="images/manifest.json">
    <link rel="mask-icon" href="images/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <style>
        @media print {
        }
    </style>
    <style>
        @media (max-width: 768px) {
        }
    </style>
    <link rel="alternate" hreflang="en" href="#.">
    <link rel="alternate" hreflang="es" href="#">
    <link rel="alternate" hreflang="de" href="#">
    <link rel="alternate" hreflang="x-default" href="#">
</head>
<body>
<div id="wrapper">
    <div id="floatingTab_1">
        <header class="header_main">
            <div class="headerNav">
                <div class="headerNav_inner clearfix">
                    <div class="navText">
                        <p>Nedbank Contact Centre    0860 114 966    Operating hours 07:00 – 21:00    365 days a year</p>
                    </div>
                    <div class="nav_socials">
                        <ul>
                            <li><a class="" href="#."><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            <li><a class="" href="#."><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a class="" href="#."><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header">
                <div class="headerAuto">
                    <div class="headerInner clearfix">
                        <div class="menuIcon"></div>
                        <div class="logo"> <a href="#."><img src="images/logo.png" alt="#"></a> </div>
                        <div class="headerRight">
                            <div class="menu">
                                <div class="menu_clos">x</div>
                                <ul>
                                    <li class="active"><a id="one_" href="#floatingTab_1">Home</a></li>
                                    <li><a id="two_" href="#floatingTab_2">POSplus</a></li>
                                    <li><a id="three_" href="#floatingTab_3">Benefits</a></li>
                                    <li><a id="four_" href="#floatingTab_4">Features</a></li>
                                    <li><a id="five_" href="#floatingTab_5">Pricing</a></li>
                                    <li><a id="six_" href="#floatingTab_6">Support</a></li>
                                </ul>
                            </div>
                            <div class="headerRight_btns clearfix">
                                <ul>
                                    <li><a href="javascript:void(0)" class="header_searchIcon"></a></li>
                                    <li><a class="headerApplyNow url_link" target="_blank" href="https://qa.bib.bizboxhub.com/public/nedbank-onboarding/public/pocketpos?tab=2">Apply now</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="searchPopup_main">
                    <div class="autoContent">
                        <div class="searchPopup_inner">
                            <div class="srchBox_field">
                                <input type="text" placeholder="Search">
                            </div>
                            <em>Search for</em>
                            <p> POSplus™ is a comprehensive business management and point-of-sale suite</p>
                            <p>POSplus™ is a mobile, online point-of-sale suite </p>
                            <p>The POSplus™ product</p>
                            <p>Sell online with POSplus™ premium.</p>
                        </div>
                    </div>
                </div>


            </div>
        </header>
        <div class="hmBaner" >
            <div class="autoContent">
                <div class="hmBanerInner">
                    <div class="hmBaner_image"> <span class=""><img class="" src="images/home_banerImage22.png" alt="#"></span> </div>
                    <div class="hmBaner_content">
                        <h2><strong>POSplus™ </strong> is a comprehensive business management and point-of-sale suite, suitable for any business.</h2>
                        <p>POSplus™ allows you to accept payments easily and manage your staff and customers efficiently.</p>
                        <a class="ApplyNow_mainButton url_link" target="_blank" href="https://qa.bib.bizboxhub.com/public/nedbank-onboarding/public/pocketpos?tab=2">Apply now</a>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="products_main" id="floatingTab_2">
            <div class="autoContent">
                <div class="products_inner">
                    <div class="productsUp">
                        <h1 class="mainHeading">The POSplus™ Product</h1>
                        <h2>POSplus™ is a mobile, online point-of-sale suite that makes it easy to sell to your customers anywhere, and to be in control of your business all the time.</h2>
                        <a class="ApplyNow_mainButton" href="products.html">Find out more</a>
                    </div>
                    <div class="hmPro_tabsOuter">
                        <div class="hmPro_tabsTitle clearfix">
                            <ul class="clearfix"  data-sequence="1000">
                                <li class="active animatedParent" >
                                    <div class="hmPro_tabsTitle_inner animated fadeIn" data-rel=".hmPro_tabsShow_1">
                                        <div class="hmPro_tabTitle_image"> <span><img src="images/hmPro_tabIcon1.png" alt="#"></span> </div>
                                        <h3>Point-of-sale app</h3>
                                        <p>Process card payments, manage your stock and track sales.</p>
                                        <a href="products.html#product_section1">Learn more</a>
                                    </div>
                                </li>
                                <li class="animatedParent">
                                    <div class="hmPro_tabsTitle_inner animated fadeIn" data-rel=".hmPro_tabsShow_2" style="-webkit-animation-delay:0.3s;animation-delay:0.3s;">
                                        <div class="hmPro_tabTitle_image"> <span><img src="images/hmPro_tabIcon2.png" alt="#"></span> </div>
                                        <h3>Card acceptance device</h3>
                                        <p>Accept all card payments and grow your business.</p>
                                        <a href="products.html#product_section2">Learn more</a>
                                    </div>
                                </li>
                                <li class="animatedParent">
                                    <div class="hmPro_tabsTitle_inner animated fadeIn" data-rel=".hmPro_tabsShow_3" style="-webkit-animation-delay:0.6s;animation-delay:0.6s;">
                                        <div class="hmPro_tabTitle_image"> <span><img src="images/hmPro_tabIcon3.png" alt="#"></span> </div>
                                        <h3>Business portal </h3>
                                        <p>Dynamic business insights are available in the cloud, at your finger tips.</p>
                                        <a href="products.html#product_section3">Learn more</a>
                                    </div>
                                </li>
                                <li class="animatedParent">
                                    <div class="hmPro_tabsTitle_inner animated fadeIn" data-rel=".hmPro_tabsShow_4" style="-webkit-animation-delay:0.9s;animation-delay:0.9s;">
                                        <div class="hmPro_tabTitle_image"> <span><img src="images/hmPro_tabIcon4.png" alt="#"></span> </div>
                                        <h3>E-store</h3>
                                        <p>Showcase your business to the world. Trade anywhere, anytime. Sell online with POSplus™ premium.</p>
                                        <a href="products.html#product_section4">Learn more</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="hmPro_tabsShow_main">
                            <div class="hmPro_tabsShow hmPro_tabsShow_1" style="display:block">
                                <div class="hmPro_tabsShowPic"> <img src="images/hmPro_tabShow_pic1.png" alt="#"> </div>
                                <p>Engage with your customers, keep track of your business and experience the world of mobile sales.</p>
                            </div>
                            <div class="hmPro_tabsShow hmPro_tabsShow_2">
                                <div class="hmPro_tabsShowPic"> <img src="images/hmPro_tabShow_pic2.png" alt="#"> </div>
                                <p>POSplus™. Boost your sales with a low-cost payment option, offering convenience to customers.</p>
                            </div>
                            <div class="hmPro_tabsShow hmPro_tabsShow_3">
                                <div class="hmPro_tabsShowPic"> <img src="images/hmPro_tabShow_pic3.png" alt="#"> </div>
                                <p>Monitor and track your business with detailed information.</p>
                            </div>
                            <div class="hmPro_tabsShow hmPro_tabsShow_4">
                                <div class="hmPro_tabsShowPic"> <img src="images/hmPro_tabShow_pic4.png" alt="#"> </div>
                                <p>Gain higher conversions in your online store.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="benefits_main" id="floatingTab_3">
            <div class="autoContent">
                <div class="benefits_inner">
                    <h1 class="mainHeading">POSplus™ benefits</h1>
                    <div class="benefits_listing">
                        <ul>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon1.png" alt="#"> </figure>
                </i> </span>
                                    <h2><strong> 01.</strong> Cloud-based</h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p> Trade instore, online or through the mobile app. POSplus™ cloud-based technology allows you to manage all aspects of your business in your own business portal, no matter where you are.</p>
                                </div>
                            </li>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon2.png" alt="#"> </figure>
                </i> </span>
                                    <h2> <strong>02.</strong> Customer-focused</h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p> Use a wide selection of features, such as inventory and customer management, customer profile management, customer transactional history and customer offers, to help drive revenue and enhance customer engagement. Grow your business by retaining existing customers and gaining new ones.</p>
                                </div>
                            </li>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon3.png" alt="#"> </figure>
                </i> </span>
                                    <h2> <strong>03.</strong> Flexible</h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p>POSplus™ can be deployed rapidly and scaled immediately so you can efficiently run one or many businesses across the retail and hospitality industries.</p>
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon4.png" alt="#"> </figure>
                </i> </span>
                                    <h2> <strong>04.</strong> Customisable</h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p> Make it yours! Modify the business portal and customer-facing elements, such as the POS app and the e-commerce store, to represent your brand.</p>
                                </div>
                            </li>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon5.png" alt="#"> </figure>
                </i> </span>
                                    <h2> <strong>05.</strong> Operates 24/7</h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p> Showcase your business to the world and trade all hours of the day and night, with your own online e-store.</p>
                                </div>
                            </li>
                            <li>
                                <div class="benefits_integratedTitle"> <span> <i>
                <figure><img src="images/benefits_integratedIcon6.png" alt="#"> </figure>
                </i> </span>
                                    <h2> <strong>06.</strong> Insightful </h2>
                                </div>
                                <div class="benefits_integratedTxt">
                                    <p> Know and grow your business. Insightful analytics allow you to make informed business decisions to help grow your brand.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature_main" id="floatingTab_4">
            <div class="autoContent">
                <div class="feature_inner">
                    <h1 class="mainHeading">POSplus™ features</h1>
                    <div class="animatedParent">
                        <div class="lmFeatures_tabsOuter animated fadeInUpShort">
                            <div class="lmFeatures_tabs_tile clearfix">
                                <ul>
                                    <li><a class="lmFeatures_tabs_button active" href="#lmFeatures_tabs_1">Manage</a></li>
                                    <li><a class="lmFeatures_tabs_button" href="#lmFeatures_tabs_2">Sell</a></li>
                                    <li><a class="lmFeatures_tabs_button" href="#lmFeatures_tabs_3">Transact</a></li>
                                    <li><a class="lmFeatures_tabs_button" href="#lmFeatures_tabs_4">Deliver</a></li>
                                </ul>
                            </div>
                            <div class="lmFeatures_tabs_showOut">
                                <div class="lmFeatures_tabs_show" id="lmFeatures_tabs_1" style="display: block;">
                                    <div class="lmFeatures_tabs_showInnr clearfix">
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon1.png" alt="#"> </figure>
                        </i> </span> Business portal </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Easy access from the web.</li>
                                                    <li>Analytics and reporting, which can be easily printed and exported.</li>
                                                    <li>Quick-view graph and chart analytics.</li>
                                                    <li>Storing settings.</li>
                                                    <li>Cash drawer and printer support.</li>
                                                    <li>Customise receipts and email invoices.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon2.png" alt="#"> </figure>
                        </i> </span> Dashboard </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Total sales and transactions.</li>
                                                    <li>Total customers.</li>
                                                    <li>Date and time.</li>
                                                    <li>Daily comparisons.</li>
                                                    <li>Top selling items.</li>
                                                    <li>Voucher redemption and expiry details.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon3.png" alt="#"> </figure>
                        </i> </span> Reporting </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Sales summary and trends.</li>
                                                    <li>Payment methods.</li>
                                                    <li>Item and Category sales.</li>
                                                    <li>Taxes.</li>
                                                    <li>Order and Invoice reports.</li>
                                                    <li>Sale and refund report.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon4.png" alt="#"> </figure>
                        </i> </span> Staff management </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Adding and managing users.</li>
                                                    <li>Employee PIN access.</li>
                                                    <li>Shift management. </li>
                                                    <li>Track employee performance.</li>
                                                    <li>Assigning levels of access.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon5.png" alt="#"> </figure>
                        </i> </span> Customer management </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Adding new customers.</li>
                                                    <li>Exporting customer lists.</li>
                                                    <li>Knowing your customer’s name, email adress, cell number, address.</li>
                                                    <li>Customer sales history.</li>
                                                    <li>Customer visit information.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_manage_hdngIcon6.png" alt="#"> </figure>
                        </i> </span> Inventory </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Managing small to large inventories.</li>
                                                    <li>Adding, updating and remove products.</li>
                                                    <li>Stocktake and stock alerts.</li>
                                                    <li>Quick category selection.</li>
                                                    <li>Tracking costs and profit per item.</li>
                                                    <li>Easily creating modifiers and variants.</li>
                                                    <li>Presetting discounts per line and per sale.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="lmFeatures_tabs_show" id="lmFeatures_tabs_2">
                                    <div class="lmFeatures_tabs_showInnr clearfix">
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_sell_hdngIcon1.png" alt="#"> </figure>
                        </i> </span> Orders </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Place orders through the POS app, mobile app, or through the e-store.</li>
                                                    <li>Order redemption cycle syncs across all selling platforms.</li>
                                                    <li>Localise features such as country, currency, time and VAT.</li>
                                                    <li>Receive order alerts by email and through the POSplus business portal.</li>
                                                    <li>Preorder and prebook.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_sell_hdngIcon2.png" alt="#"> </figure>
                        </i> </span> Payments </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Hold transactions.</li>
                                                    <li>Refund transactions.</li>
                                                    <li>Print and email receipts.</li>
                                                    <li>Accept cash, cards, masterpass, points and / <br> or vouchers.</li>
                                                    <li>Make use of the tip functionality.</li>
                                                    <li>Continue to sell when offline – transactions sync once you're back online.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_sell_hdngIcon3.png" alt="#"> </figure>
                        </i> </span> Cart and checkout</h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Enjoy simple cart and tally functions.</li>
                                                    <li>Use secure bank payment gateways.</li>
                                                    <li>Redeem vouchers.</li>
                                                    <li>Registered users enjoy faster checkouts.</li>
                                                    <li>Track orders.</li>
                                                    <li>Cross-sell and upsell recommended products.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_sell_hdngIcon4.png" alt="#"> </figure>
                        </i> </span> Split tender and Multi tender </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Multitender – Customers can pay for a single transaction using multiple payment types such as a card, cash and gift cards.</li>
                                                    <li>Split tender – Allow a group of people to pay one bill in parts using different payment types.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_connect_hdngIcon1.png" alt="#"> </figure>
                        </i> </span> E-store </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>Your own online shop is linked directly to your POS app and business portal.</li>
                                                    <li>Customise e-store theme (colours, fonts, footers, banners, etc).</li>
                                                    <li>Enjoy a complete content management system.</li>
                                                    <li>Set up infinite offers and promotions.</li>
                                                    <li>View reports (total e-store sales, item sales, customer orders).</li>
                                                    <li>Add featured products, collections and content to your home page.</li>
                                                    <li>Send newsletters to customers who opt in.</li>
                                                    <li>Keep customers updated by posting blogs.</li>
                                                    <li>Allow sharing on social media (instagram, facebook, twitter, pinterest).</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_sell_hdngIcon6.png" alt="#"> </figure>
                        </i> </span> Vouchers and gift cards </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <ul>
                                                    <li>From your POSplus business portal, create discount vouchers (either in rand or as a percentage of purchase) for your most valued customers, or run promotions for a set period. Customers can easily enter the voucher code when they check out online or on your POS device.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="lmFeatures_tabs_show" id="lmFeatures_tabs_3">
                                    <div class="lmFeatures_tabs_showInnr clearfix">
                                        <div class="lm_transctDevice_outer">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_acceptPayment_hdngIcon1.png" alt="#"> </figure>
                        </i> </span> Accept all payment types </h2>
                                            </div>
                                            <div class="transactTab_content clearfix">
                                                <div class="transactTab_left">
                                                    <ul>
                                                        <li>
                                                            <div class="transactTaype_box">
                                                                <a href="#.">
                                                                    <figure>
                                                                        <img src="images/acptPayment_icon2.png" alt="#">
                                                                    </figure>
                                                                </a>

                                                            </div>
                                                            <p>Cash</p>
                                                        </li>
                                                        <li>
                                                            <div class="transactTaype_box">
                                                                <a href="#.">
                                                                    <figure>
                                                                        <img src="images/acptPayment_icon1.png" alt="#">
                                                                    </figure>
                                                                </a>

                                                            </div>
                                                            <p> Vouchers</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="transactTab_right">
                                                    <div class="lm_transctDevicePics">
                                                        <ul>
                                                            <li> <span><img src="images/paymentType_mainPic.png" alt="#"></span> </li>
                                                        </ul>
                                                    </div>
                                                    <div class="lm_transctDevice_payLogos clearfix">
                                                        <ul>
                                                            <li><a class="lm_transctDevice_payLogos_logo1" href="#."></a></li>
                                                            <li><a class="lm_transctDevice_payLogos_logo2" href="#."></a></li>
                                                            <li><a class="lm_transctDevice_payLogos_logo3" href="#."></a></li>
                                                            <li><a class="lm_transctDevice_payLogos_logo2" href="#."></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="lmFeatures_tabs_show" id="lmFeatures_tabs_4">
                                    <div class="lmFeatures_tabs_showInnr clearfix">
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_connect_hdngIcon3.png" alt="#"> </figure>
                        </i> </span> Shipping and deliveries</h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <p>You'll be able to generate PDF labels with tracking numbers in the size you want. Once a label has been generated, its fulfilment status, tracking number and the courier name can be viewed in your business portal. Your customers will be able to calculate shipping rates and get available service types and expected delivery times on checkout. You can set up individual profiles for each shipper account and have multiple shipper accounts under the same courier.</p>
                                            </div>
                                            <div class="supportedCouriers_main">
                                                <h3>Supported couriers:</h3>
                                                <ul>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon1.png" alt="#"></span> <strong>UPS</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon2.png" alt="#"></span> <strong>FedEx</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon3.png" alt="#"></span> <strong>DHL Express</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon4.png" alt="#"></span> <strong>TNT</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon5.png" alt="#"></span> <strong>Aramex</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon6.png" alt="#"></span> <strong>Bpost International</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon7.png" alt="#"></span> <strong>DTDC</strong> </div>
                                                    </li>
                                                    <li>
                                                        <div class="courier_box"> <span><img src="images/courier_icon8.png" alt="#"></span> <strong>SkyNet Worldwide</strong> </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lmFeatures_tabs_showBox">
                                            <div class="lmFeatures_manage_hdng">
                                                <h2> <span> <i>
                        <figure> <img src="images/lmFeatures_connect_hdngIcon2.png" alt="#"> </figure>
                        </i> </span> Pre-order through the mobile app </h2>
                                            </div>
                                            <div class="lmFeatures_reportingListig">
                                                <p>Customers can conveniently order ahead and prepurchase products through the mobile app, allowing your staff to plan ahead as the POS app is fully integrated and receives an alert as soon as an order is made from the mobile app.
                                                    <br><br>
                                                    Please contact <strong>Nedbank Contact Centre 0860 114 966</strong> for more information.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sliderAds_outer">
            <div class="sliderAds_innr">
                <ul class="bxslider">
                    <li>
                        <div class="sliderAds_run sliderAds_run2nd clearfix">
                            <div class="sliderAds_left">
                                <div class="sliderAds_leftInner">
                                    <div class="sliderAds_locIocn"> <span><img src="images/sliderAds_locIocn.png" alt="#"></span> </div>
                                    <div class="sliderAds_textHdngs">
                                        <h3>Manage your business in one place</h3>
                                        <h1>Get started with POSplus™</h1>
                                        <p>Start using POSplus™ today. Manage your customers, add and delete inventory, view real-time transaction reports, send vouchers to your customers and so much more.</p>
                                    </div>
                                    <div class="sliderAds_learnUpdates_btnsOut">
                                        <ul>
                                            <li><a class="sliderAds_learnUpdates_btn active url_link" target="_blank" href="https://qa.bib.bizboxhub.com/public/nedbank-onboarding/public/pocketpos?tab=2">Apply Now</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="sliderAds_right animatedParent"> <span class="animated fadeInRight"><img src="images/sliderAds_pic2.png" alt="#"></span> </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="pricingContentOut" id="floatingTab_5">
            <div class="pricingContentInnOut">
                <div class="autoContent priceSection">
                    <div class="pricingContentInn">
                        <h1 class="mainHeading">Pricing</h1>
                        <div class="pricingGettoday_out pricingChanges">
                            <div class="pricingGettoday_right clearfix">
                                <ul>
                                    <li>
                                        <div class="pricingGettoday_box ">
                                            <!-- <input type="radio" name="priceChck_radio" class="pricingGettoday_chckRadio">-->
                                            <div class="pricingGettoday_hdr cardHead">
                                                <p>PocketPOS™ card reader</p>
                                            </div>
                                            <div class="pricingGettoday_left ">
                                                <div class="pricingLb_device clearfix"> <h1>R2 399 <small>(incl VAT)</small></h1><span class=""><img src="images/pricingLb_device.png" alt="#"></span> </div>

                                                <div class="calculated_values">
                                                    <span>+ R115 once off admin fee <small>incl VAT.</small></span>
                                                    <span>Merchant service commission 2,75% <small>excl VAT</small></span>
                                                    <p>Calculated on transaction value.</p>
                                                </div>


                                                <h6 style="text-align:left;">No minimum period contract</h6>
                                                <p style="text-align:left;">If you are not entirely satisfied with your PocketPOS™ card acceptance device, return it within 30 days and we will refund you (excluding once-off non-refundable admin fee of R115).  </p>
                                                <p class="pricingBadge"><strong>WARRANTY:</strong> Includes six month warranty on workmanship and material.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="orIcon">
                                        <div class="pricingGettoday_box ">
                                            <!--<input type="radio" name="priceChck_radio" class="pricingGettoday_chckRadio">-->
                                            <div class="pricingGettoday_hdr liteHead">
                                                <p>POSplus™ LITE</p>
                                            </div>
                                            <div class="pricingGettoday_boxCont litePackege_setting">
                                                <h2>Free  </h2>

                                                <div class="pricingGettoday_priceBox_lstng">
                                                    <ul>
                                                        <li><span>Inventory management</span></li>
                                                        <li><span>Customer information management</span></li>
                                                        <li><span>Cloud-based point-of-sale app</span></li>
                                                        <li><span>Business portal</span></li>
                                                        <li><span>Accepting all card payments (American Express, Visa, Mastercard)</span></li>
                                                        <li><span>Accepting Masterpass</span></li>
                                                        <li><span>Fast processing with Tap 'n Go</span></li>
                                                        <li><span>Digital receipts</span></li>
                                                        <li><span>Staff management</span></li>
                                                        <li><span>Support from Nedbank Contact Centre</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="pricingGettoday_box active">
                                            <!--<input type="radio" name="priceChck_radio" class="pricingGettoday_chckRadio">-->
                                            <div class="pricingGettoday_hdr">
                                                <p>POSplus™ PREMIUM</p>
                                            </div>
                                            <div class="pricingGettoday_boxCont pemium_setting">
                                                <h2>R400 <small> per month </small></h2>

                                                <div class="pricingGettoday_priceBox_lstng">
                                                    <strong class="pricingRates pad_bottom10 border0">ALL LITE FEATURES <em>PLUS</em></strong>
                                                    <ul>
                                                        <li><span>Online e-store and website</span></li>
                                                        <li><span>Adding multiple businesses</span></li>
                                                        <li><span>Adding multiple registers</span></li>
                                                        <li><span>Voucher issue and redemption</span></li>
                                                        <li><span>Tracking employee performance</span></li>
                                                        <li><span>Detailed insight and reporting</span></li>
                                                        <li><span>Advanced inventory management</span></li>
                                                        <li><span>Customer loyalty offers</span></li>
                                                        <li><span>Integration into accounting system</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>



                            </div>


                            <div class="applyBttn">
                                <a href="https://qa.bib.bizboxhub.com/public/nedbank-onboarding/public/pocketpos?tab=2" target="_blank" class="ApplyNow_mainButton url_link">Apply now</a>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="retailStore_main">
            <div class="retailStore_slider">
                <div class="retailStore_sliderrun">
                    <ul class="retail_slider">
                        <li>
                            <div class="retailStore_inner">
                                <div class="retailStore_image">
                                    <div class="web_retailStore_image"><img src="images/retailStore_sliderPic1.png" alt="#"></div>
                                    <div class="mob_retailStore_image"><img src="images/retailStore_sliderPic1_mobile.png" alt="#"></div>
                                </div>
                                <div class="retailStore_pos">
                                    <div class="retailStore_posTbl">
                                        <div class="retailStore_posCell">
                                            <div class="autoContent">
                                                <div class="retailStore_data clearfix">
                                                    <div class="retailStore_circleImage"> <span>
                          <figure> <img src="images/industries_usedPic_typeIcon1.png" alt="#"> </figure>
                          </span> </div>
                                                    <div class="retailStore_dataRight">
                                                        <div class="">
                                                            <h2>POSplus™ can be used for any business that requires a point-of-sale suite.</h2>
                                                            <h3>Are you in the hospitatilty or food industry?</h3>
                                                            <ul>
                                                                <li><a href="#.">Cafes</a></li>
                                                                <li><a href="#."> Bars and clubs</a></li>
                                                                <li><a href="#.">Restaurants</a></li>
                                                                <li><a href="#.">Hotels</a></li>
                                                                <li><a href="#.">Catering businesses</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="retailStore_inner">
                                <div class="retailStore_image">
                                    <div class="web_retailStore_image"><img src="images/retailStore_sliderPic2.png" alt="#"></div>
                                    <div class="mob_retailStore_image"><img src="images/retailStore_sliderPic2_mobile.png" alt="#"></div>
                                </div>
                                <div class="retailStore_pos">
                                    <div class="retailStore_posTbl">
                                        <div class="retailStore_posCell">
                                            <div class="autoContent">
                                                <div class="retailStore_data clearfix">
                                                    <div class="retailStore_circleImage"> <span>
                          <figure> <img src="images/heartIcon.png" alt="#"> </figure>
                          </span> </div>
                                                    <div class="retailStore_dataRight">
                                                        <div class="">
                                                            <h2>POSplus™ can be used for any business that requires a point-of-sale suite.</h2>
                                                            <h3>Are you in the health or beauty business?</h3>
                                                            <ul>
                                                                <li><a href="#.">Fitness and supplement stores</a></li>
                                                                <li><a href="#.">Hair and beauty stores</a></li>
                                                                <li><a href="#.">Hair salons</a></li>
                                                                <li><a href="#.">Veterinarians</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="retailStore_inner">
                                <div class="retailStore_image">
                                    <div class="web_retailStore_image"><img src="images/retailStore_sliderPic3.png" alt="#"></div>
                                    <div class="mob_retailStore_image"><img src="images/retailStore_sliderPic3_mobile.png" alt="#"></div>
                                </div>
                                <div class="retailStore_pos">
                                    <div class="retailStore_posTbl">
                                        <div class="retailStore_posCell">
                                            <div class="autoContent">
                                                <div class="retailStore_data clearfix">
                                                    <div class="retailStore_circleImage"> <span>
                          <figure> <img src="images/drillIcon.png" alt="#"> </figure>
                          </span> </div>
                                                    <div class="retailStore_dataRight">
                                                        <div class="">
                                                            <h2>POSplus™ can be used for any business that requires a point-of-sale suite.</h2>
                                                            <h3>Are you in the retail business?</h3>
                                                            <ul>
                                                                <li><a href="#.">Bike shops</a></li>
                                                                <li><a href="#.">Toy shops</a></li>
                                                                <li><a href="#.">Pet shops</a></li>
                                                                <li><a href="#.">Hardware stores</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="retailStore_inner">
                                <div class="retailStore_image">
                                    <div class="web_retailStore_image"><img src="images/retailStore_sliderPic4.png" alt="#"></div>
                                    <div class="mob_retailStore_image"><img src="images/retailStore_sliderPic4_mobile.png" alt="#"></div>
                                </div>
                                <div class="retailStore_pos">
                                    <div class="retailStore_posTbl">
                                        <div class="retailStore_posCell">
                                            <div class="autoContent">
                                                <div class="retailStore_data clearfix">
                                                    <div class="retailStore_circleImage"> <span>
                          <figure> <img src="images/educationIcon.png" alt="#"> </figure>
                          </span> </div>
                                                    <div class="retailStore_dataRight">
                                                        <div class="">
                                                            <h2>POSplus™ can be used for any business that requires a point-of-sale suite.</h2>
                                                            <h3>Are you in the corporate education business?</h3>
                                                            <ul>
                                                                <li><a href="#.">Education and institutional facilities</a></li>
                                                                <li><a href="#.">book stores</a></li>
                                                                <li><a href="#.">training facilities </a></li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="retailStore_inner">
                                <div class="retailStore_image">
                                    <div class="web_retailStore_image"><img src="images/retailStore_sliderPic5.png" alt="#"></div>
                                    <div class="mob_retailStore_image"><img src="images/retailStore_sliderPic5_mobile.png" alt="#"></div>
                                </div>
                                <div class="retailStore_pos">
                                    <div class="retailStore_posTbl">
                                        <div class="retailStore_posCell">
                                            <div class="autoContent">
                                                <div class="retailStore_data clearfix">
                                                    <div class="retailStore_circleImage"> <span>
                          <figure> <img src="images/hangeClothIcon.png" alt="#"> </figure>
                          </span> </div>
                                                    <div class="retailStore_dataRight">
                                                        <div class="">
                                                            <h2>POSplus™ can be used for any business that requires a point-of-sale suite.</h2>
                                                            <h3>Are you in the retail business?</h3>
                                                            <ul>
                                                                <li><a href="#.">Cafes</a></li>
                                                                <li><a href="#.">Bars and clubs</a></li>
                                                                <li><a href="#.">Restaurants</a></li>
                                                                <li><a href="#.">Bakeries and delis</a></li>
                                                                <li><a href="#.">Markets (food, beverage, fashion, gifts, etc)</a></li>
                                                                <li><a href="#.">Takeaway shops</a></li>
                                                                <li><a href="#.">Florists</a></li>
                                                                <li><a href="#.">Fashion stores</a></li>
                                                                <li><a href="#.">Homeware and gift shops</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sliderAds_outer">
            <div class="sliderAds_innr">
                <ul class="bxslider">
                    <li>
                        <div class="sliderAds_run sliderAds_run2nd clearfix">
                            <div class="sliderAds_left">
                                <div class="sliderAds_leftInner">
                                    <div class="sliderAds_locIocn"> <span><img src="images/sliderAds_locIocn2.png" alt="#"></span> </div>
                                    <div class="sliderAds_textHdngs">
                                        <h3>Realise the full potential of your business</h3>
                                        <h1>Get started with POSplus™</h1>
                                        <p>POSplus™  provides easy-to-use touchscreen technology and some of the most powerful functionalities seen in a modern point-of-sale system. POSplus™  works on Android or iOS mobile devices. </p>
                                    </div>
                                    <div class="sliderAds_learnUpdates_btnsOut googlePlayBtns_parent">
                                        <ul>
                                            <ul>
                                                <li><a href="https://www.apple.com/lae/" class=""><img src="images/footerPlayStors_btn1.png" alt="#"></a></li>
                                                <li><a href="https://play.google.com/store?hl=en" class=""><img src="images/footerPlayStors_btn.png" alt="#"></a></li>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="sliderAds_right animatedParent"> <span class="animated fadeInRight"><img src="images/sliderAds_picNew.png" alt="#"></span> </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="ppSupport_main" id="floatingTab_6">
            <div class="autoContent">
                <div class="ppSupport_inner clearfix">
                    <div class="ppSupport_cont">
                        <h1 class="mainHeading">POSplus™ support</h1>
                        <ul>
                            <li>
                                <div class="lmFeatures_manage_hdng">
                                    <h2> <span> <i>
                  <figure> <img src="images/pp_supportIcon1.png" alt="#"> </figure>
                  </i> </span> Telephone support </h2>
                                </div>
                                <p>Nedbank Contact Centre <strong> 0860 114 966.</strong> </p>
                                <p>Operating hours 07:00–21:00, 365 days a year.</p>
                            </li>
                            <li>
                                <div class="lmFeatures_manage_hdng">
                                    <h2> <span> <i>
                  <figure> <img src="images/pp_supportIcon2.png" alt="#"> </figure>
                  </i> </span> Online chat </h2>
                                </div>
                                <p>When an agent is online, you can chat live to get immediate assistance. If no agent is online, leave a message and one will respond within 24 hours.</p>
                            </li>
                            <li>
                                <div class="lmFeatures_manage_hdng">
                                    <h2> <span> <i>
                  <figure> <img src="images/pp_supportIcon3.png" alt="#"> </figure>
                  </i> </span> Support ticket </h2>
                                </div>
                                <p>Send a support ticket by email or from your POSplus point-of-sale app or business portal under the support tab. Your support ticket is sent to a team of agents and you will receive a response with 24 hours.</p>
                            </li>

                            <li>
                                <div class="lmFeatures_manage_hdng">
                                    <h2> <span> <i>
                  <figure> <img src="images/pp_supportIcon5.png" alt="#"> </figure>
                  </i> </span> Self-service </h2>
                                </div>
                                <p> Through this <a href="https://soldi.zendesk.com">link</a>  you get access to online self-help guides, FAQs, video and tutorials.
                                </p>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footerOut" id="">
            <div id="footer">
                <div class="autoContent">
                    <div class="footerInnr">
                        <div class="footerUp">
                            <div class="footerLogo"><a href="#."><img src="images/footerLogo.png" alt="#"></a></div>
                            <div class="footerContactsoUT clearfix">
                                <div class="footerContacts">
                                    <ul>
                                        <li><strong><a href="callTo:0860 114 966">0860 114 966</a><em>Nedbank Contact Centre</em> </strong></li>
                                    </ul>
                                </div>
                                <div class="footerPlayStors_out">
                                    <div class="footerPlayStors_btnsOut clearfix">
                                        <ul>
                                            <li><a href="https://www.apple.com/lae/" class=""><img src="images/footerPlayStors_btn1.png" alt="#"></a></li>
                                            <li><a href="https://play.google.com/store?hl=en" class=""><img src="images/footerPlayStors_btn.png" alt="#"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footerbtm">
                            <div class="footerDfrntly_out clearfix">
                                <h2>See money Differently </h2>
                                <div class="footerDfrntly_innr">
                                    <div class="footer_memberLogo"> <a href="#."><img src="images/footer_memberLogo.png" alt="#"></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footerNav">
                    <div class="footerNavInner clearfix">
                        <div class="footerMenu">
                            <ul>
                                <li><a class="footerMenu_contact" href="#.">Contact</a></li>
                                <li><a class="footerMenu_atm" href="#.">Find an ATM or branch</a></li>
                                <li><a class="footerMenu_help"  href="#.">Help</a></li>
                                <li><a class="footerMenu_tc"  href="#.">Terms and conditions</a></li>
                            </ul>
                        </div>
                        <div class="footerMenu_authorised">
                            <p>Nedbank Ltd Reg No 1951/000009/06.<br>
                                Authorised financial services and registered credit provider (NCRCP16).</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/my_script_home.js"></script>
    <script type="text/javascript" src="js/external_js.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <!-- Start of plutus Zendesk Widget script -->
    <script>window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="soldi.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.bxslider').bxSlider({
                adaptiveHeight: true,
                speed:200,
                mode: 'fade',
                onSlideAfter: function(){
                    // do mind-blowing JS stuff here
                    $(".sliderAds_right span").addClass("go");
                }
            });


            $('.retail_slider').bxSlider({

                speed:1000,
                mode: 'fade',
                auto: true ,
                onSlideAfter: function(){
                    // do mind-blowing JS stuff here
                    $(".sliderAds_right span").addClass("go");
                },
                adaptiveHeight: true,
            });


        });
        $(window).load(function(e) {
            $(".feedBack_sendBtn").click(function(e) {
                $("#launcher").trigger( "click" );
            });





            $(window).scroll(function() {



            });








        });
    </script>
    <script type="text/javascript" src="js/jquery.reel.js"></script>
    <link rel="stylesheet" type="text/css" href="css/animations.css">
    <link type="text/css" href="css/animations-ie-fix.css" rel="stylesheet">
    <script type="text/javascript" src="js/css3-animate-it.js"></script>
    <script type="text/javascript" src="js/paralex_custom.js"></script>
</body>
</html>