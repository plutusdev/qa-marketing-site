$(document).ready(function(e) {
	
	//new js
	 $(".lmFeatures_tabs_button").click(function(index, element) {
        $(".lmFeatures_tabs_button").removeClass("active");
        $(this).addClass("active");
        var lmFeaturesBtn_hrf = $(this).attr("href");
        $(".lmFeatures_tabs_show").hide();
        $(lmFeaturesBtn_hrf).show();
        return false
     });
	 
	  $(".hmPro_tabsTitle_inner").click(function(index, element) {
        $(this).closest("ul").find("li").removeClass("active");
        $(this).parent().addClass("active");
        var hmPro_tabs = $(this).attr("data-rel");
         $(".hmPro_tabsShow").hide();
        $(hmPro_tabs).show();
       
     });
	 
	//new js end
	
	
    $("select").each(function(index, element) {
        var selectVal = $(this).find(":selected").val();
        $(this).parent().find("span").text(selectVal);
    });
    $("select").change(function(e) {
        var srchFiltr_val = $(this).find(":selected").val();
        $(this).parent().find("span").text(srchFiltr_val);
    });
    $(".pos_tememberMe_chckbox input").click(function(index, element) {
        if ($(this).prop("checked") == true) {
            $(this).parent().addClass("checked");
        } else {
            $(this).parent().removeClass("checked");
        }
    });
    $(".posSignIn_tabsOut ul li a").click(function(index, element) {
        $(".posSignIn_tabsOut ul li").removeClass("active");
        $(this).parent().addClass("active");
        var signBtn_hrf = $(this).attr("href");
        $(".posSignIn_showCont_out").hide();
        $(signBtn_hrf).show();
        return false
    });
    $(".leftTbs_btnsOut ul li a").click(function(index, element) {
        return false
    });
	
    $(".menu ul li a, .products_menuInner ul li a").click(function() {
        $(".menu ul li, .products_menuInner ul li").removeClass("active");
        $(this).parent().addClass("active");
        $(".headerRight").removeClass("open_menu");
        $("body").removeClass("opne_mobile_menu");
        menuIcon_status = 1;
        hdrHeight = $('.header').height();
        var lftBr_dot_hrf = $(this).attr("href");
        var position = $(lftBr_dot_hrf).offset().top - hdrHeight + 20;
        var body = $("html, body");
        body.animate({
            scrollTop: position
        }, 800);
        return false;
    });
	
	
    var menuIcon_status = 1;
    $(".menuIcon").click(function() {
        if (menuIcon_status == 1) {
            $(".headerRight").addClass("open_menu");
            $("body").addClass("opne_mobile_menu");
            menuIcon_status = 0;
        } else {
            $(".headerRight").removeClass("open_menu");
            $("body").removeClass("opne_mobile_menu");
            menuIcon_status = 1;
        }
    });
    $(".menu_clos, .content, .footerOut, .banerContent_out, .header_searchIcon").click(function() {
        $(".headerRight").removeClass("open_menu");
        $("body").removeClass("opne_mobile_menu");
        menuIcon_status = 1;
    });
    $(".pricingGettoday_chckRadio").click(function(e) {
        $(".pricingGettoday_box").removeClass("active");
        $(this).parents(".pricingGettoday_box").addClass("active");
    });
	
	
	
	 /*$(".pricingGettoday_box").hover(function() {
        $(".pricingGettoday_box").removeClass("active");
        $(this).addClass("active");
     });*/
	 
	 
	 
	 $(".pricingGettoday_box").hover(function() {
        $(".pricingGettoday_box").removeClass("activeColor");
        $(this).addClass("activeColor");
     });
	
	
	
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(function() {
            $('.captionWrapper.valign').css({
                top: '0px'
            });
            $('.parallaxLetter').css({
                display: 'none'
            });
        });
    } else {
        $(window).stellar({
            responsive: true,
            horizontalOffset: 0,
            horizontalScrolling: false
        });
    }
	
	
    var autoOfset = $(".autoContent").offset().left;
    var autoOfsetWdth = $(".autoContent").width();
    $(".leftTbs_btnsOut").css("left", autoOfset);
	
    $(".footer_chatIcon_out").css("left", autoOfset + autoOfsetWdth - 38);
    $(".footer_chatIcon_out").addClass("visible");
	
    var secWindHeight = $(window).height();
    $(".floatingSection").each(function(index, element) {
        $(this).css("min-height", secWindHeight);
    });
	
	var headerMain_height = $(".header_main").height();
	$("#wrapper").css("padding-top",headerMain_height);




    $("body").on("click",".header_searchIcon",function(e){
		 e.stopPropagation();
		 $(".searchPopup_main").slideDown();
	});
	
	 $("body").on("click",".searchPopup_main",function(e){
		 e.stopPropagation();
	 });
	
  	$(document).on('click', function() {
		 $(".searchPopup_main").slideUp();
	});




	
 
});


$(window).scroll(function() {
	
    var sticky = $('.header_main, #wrapper');
    scroll = $(window).scrollTop();
    if (scroll >= 10) {
        sticky.addClass('fixed');
		

        setTimeout(function() {
            sticky.addClass('fixdAfter');
        }, 100);
    } else {
        sticky.removeClass('fixed fixdAfter');
    }
	
	
	try{
		var proMenuOffset = $(".products_parent").offset().top;
		if (scroll >= proMenuOffset) {
			$(".products_menu").addClass('fixed');
			$(".searchPopup_main").hide();
		 } else {
			$(".products_menu").removeClass('fixed fixdAfter');
		 }
	}catch(ex){
	}
	
	
	//home page panels
	try{
		var topper = $(document).scrollTop();
		var one_panel1 = $('#floatingTab_1').offset().top - 200;
		var two_panel2 = $('#floatingTab_2').offset().top - 200;
		var three_panel3 = $('#floatingTab_3').offset().top - 200;
		var four_panel4 = $('#floatingTab_4').offset().top - 200;
		var five_panel5 = $('#floatingTab_5').offset().top - 200;
		var six_panel6 = $('#floatingTab_6').offset().top - 200;
		
		$(".menu ul li").removeClass("active");
		
		if (topper < two_panel2) {
			$("#one_").parent().addClass("active");
		} else if (topper > one_panel1 && topper < three_panel3) {
			$("#two_").parent().addClass("active");
		} else if (topper > two_panel2  && topper < four_panel4) {
			$("#three_").parent().addClass("active");
		} 
		else if (topper > three_panel3  && topper < five_panel5) {
			$("#four_").parent().addClass("active");
		} 
		else if (topper > four_panel4  && topper < six_panel6) {
			$("#five_").parent().addClass("active");
		} 
		else if (topper > five_panel5) {
			$("#six_").parent().addClass("active");
		} 

	}catch(ex){
	}
	//end home page panels
	
	
	try{
		 var topper = $(document).scrollTop();
		 var one_panel = $('#product_section1').offset().top - 200;
		 var two_panel = $('#product_section2').offset().top - 200;
		 var three_panel = $('#product_section3').offset().top - 200;
		 var four_panel = $('#product_section4').offset().top - 200;
 		
		$(".products_menuInner ul li").removeClass("active");
	   
		if (topper < two_panel) {
			$("#proMenu_1").parent().addClass("active");
		} else if (topper > one_panel && topper < three_panel) {
			$("#proMenu_2").parent().addClass("active");
		}else if (topper > two_panel && topper < four_panel) {
			$("#proMenu_3").parent().addClass("active");
		} else if (topper > three_panel) {
			$("#proMenu_4").parent().addClass("active");
		} 
	}catch(ex){
	}
 
 
 
});



$(window).load(function(e) {
	
	var headerMain_height = $(".header_main").height();
	$("#wrapper").css("padding-top",headerMain_height);
	 //$("#wrapper").css("opacity", "1");
	
  /*  $(".posSplash_contOut").fadeOut();
    $("#wrapper").css("opacity", "1");
    $("body").removeClass("hidden");
    var autoOfset = $(".autoContent").offset().left;
    var autoOfsetWdth = $(".autoContent").width();
    $(".leftTbs_btnsOut").css("left", autoOfset);
    $(".footer_chatIcon_out").css("left", autoOfset + autoOfsetWdth - 35);
    $(".footer_chatIcon_out").addClass("visible");
    var secTw_ofst = $("#floatingTab_2").offset().top;
    var solutionLog_picHeight = $(".solutionLog_pic").height();
    var newVr = secTw_ofst - solutionLog_picHeight;
    $(".solutionLog_pic").css("top", newVr - 20);*/
});
$(window).resize(function(e) {
	var headerMain_height = $(".header_main").height();
	$("#wrapper").css("padding-top",headerMain_height);

  /*  var autoOfset = $(".autoContent").offset().left;
    var autoOfsetWdth = $(".autoContent").width();
    $(".leftTbs_btnsOut").css("left", autoOfset);
    $(".footer_chatIcon_out").css("left", autoOfset + autoOfsetWdth - 35);
    $(".footer_chatIcon_out").addClass("visible");
    var secWindHeight = $(window).height();
    $(".floatingSection").each(function(index, element) {
        $(this).css("min-height", secWindHeight);
    });
    var secTw_ofst = $("#floatingTab_2").offset().top;
    var solutionLog_picHeight = $(".solutionLog_pic").height();
    var newVr = secTw_ofst - solutionLog_picHeight;
    $(".solutionLog_pic").css("top", newVr - 20);*/
});



function clearText(field) {
    if (field.defaultValue == field.value) {
        field.value = "";
    } else if (field.value == "") {
        field.value = field.defaultValue;
    }
}